#!/usr/bin/python3

# Este script servirá para meter um url do youtube ou um termo de procura, e a
# partir daí fazer play da próxima canção sugerida.

# Versão 5.6
# 2021-03-24
# - Criação da função "print_cool_title2()", mais simples e graficamente mais
#   contrastante que a "print_cool_title()".
# - Mensagens do next_vid() só em modo de debugging
# - next_vid(): retorna None, caso a lista de resultados não exista

# Versão 5.5.1
# 2020-12-20
# - Acerto da extracção da informação em json das páginas
# - Uso da informação json para o titulo e próximo vídeo
# - Converversão de "code" para "videoId", "playlistId" ou "itemId" conforme o
#   contexto.
# - Remoção da função "parse_html_chars()", dado que agora não é necessário
#   extrair informação directamente do html

# Versão 5.5
# 2020-07-23
# - Aceitação da nova versão do html do youtube
# - Limpeza de código morto

# Versão 5.4.4
# - Verifica a existência dos programas externos utilizados

# Versão 5.4.3
# - Correcção no next_vid, tal que ao não encontrar o código do próximo vídeo,
#   devolve um None.
# - Se o play_vid tiver um None como código de entrada, simplesmente pára o
#   programa.

# Versão 5.4.2
# - Aceitação do endereço curto do youtube (ex:https://youtu.be/QA483lIvL-4)

# Versão 5.4.1
# - Nova função exit_soft(), a qual inclui a limpeza do thumbnail da pasta
#   /tmp.
# - Nova variável global "thumb_fn" e nova função thumb_fn_new() para gerar um
#   nome único, reduzindo o perigo de colisão com outro ficheiro.

# FIXME: Separar a funcionalidade de play_vid da funcionalidade de procurar o
#        próximo vídeo. Uma coisa não tem nada a haver com a outra
# FIXME: Provavelmente terei de reformular a arquitectura do programa e
#        clarificar funcionalidades
# FIXME: Unificar as mensagens de output.
#        Unificar nomes de variáveis (ex: txt/html), (ex: code/videoid)

# FIX:
# - centralizar os exits, mensagens de despedida e limpezas finais
# 	- ver play_vid() e user_make_choice()

# Mais alterações a fazer:
# - Aceitar playlists (listas de videos)
#	- Seja um ficheiro ou no stdin
# - Construir um interface em ncurses ? que tenha keybinding tipo cmus
# - Consolidar o código
# 	- Quais são as funções atómicas?
#	- Reescrever as funções em função das funções atómicas
#	- Usar variáveis com nomes significativos (ex: txt vs html)
# - Lidar com o ctrl+c com um handler de sinais
# - Tentar livrar-me das excepções
# - A função de escolha de um item deve ser generalizada.
#	- O input deve ser uma lista de strings e o output deve ser o índice da
#     escolha

import sys
import os
import termios
import subprocess as sp
import urllib.request
import urllib
import shutil
from math import ceil
import json

# generates the thumbnail filename
def thumb_fn_new():
	pid = os.getpid()
	thumb_fn = "/tmp/hqdefault-%i.jpg"%pid
	return thumb_fn


# Cleanup and exit
def exit_soft(exit_code):
	global thumb_fn
	if os.path.exists(thumb_fn):
		os.remove(thumb_fn)
	print("[radio-yt] Goodbye")
	exit(exit_code)


# Verifies the existence of a given program name
def program_exist(progname):
	return shutil.which(progname) != None


# Execução de um comando com a bibilioteca "subprocess"
def run_process(cmd_list, background=False):
	p = sp.Popen(cmd_list, stdout=sp.PIPE, stderr=sp.PIPE)
	if not background:
		p.wait()
	out,err = p.communicate()
	return out, err


def download_file(url, filename):
	r = urllib.request.urlopen(url)
	binary = r.read()
	fp = open(filename, "wb")
	fp.write(binary)
	fp.close()


def download_page(url):
	page = urllib.request.urlopen(url)
	html = page.read().decode()
	return html


def get_term_width():
	cols = shutil.get_terminal_size()[0]
	return cols


def get_term_height():
	lines = shutil.get_terminal_size()[1]
	return lines


def videoId2url(videoId):
	return "https://www.youtube.com/watch?v=" + videoId


def itemId2url_img(itemId):
	return "https://i.ytimg.com/vi/"+ itemId + "/hqdefault.jpg"


def html_get_datajson(html):
	beginStr = ">var ytInitialData = {"
	endStr   = "};</script>"

	i = html.index(beginStr)
	if i == -1:
		return None
	i0 = i + len(beginStr) - 1
	
	i1 = -1
	# Bracket count
	bc = 1
	for k in range(i0+1, len(html)):
		if bc == 0:
			i1 = k
			break
		if html[k] == '}':
			bc -= 1
		elif html[k] == '{':
			bc += 1
	if i1 == -1:
		return None

	data = json.loads(html[i0:i1])
	return data



def search_choices_extract(html):
	data = html_get_datajson(html)

	resultList= data\
		["contents"]\
		["twoColumnSearchResultsRenderer"]\
		["primaryContents"]\
		["sectionListRenderer"]\
		["contents"]\
		[0]\
		["itemSectionRenderer"]\
		["contents"]

	l_choices = []
	for v in resultList:
		k = list(v.keys())[0]
		if k == "videoRenderer":
			videoId   = v[k]["videoId"]
			title     = v[k]["title"]["runs"][0]["text"]
			item_type = "vd"
			l_choices.append([videoId, title, item_type])

	return l_choices


# Input : lista de strings
# Output: índice do item escolhido
def menu_choice(l_choices):
	N = len(l_choices)

	if N==0:
		return None

	n_cols_term = get_term_width()
	n_rows_term = get_term_height()

	# 3 columas = 2 espaço + 1 char de dois pontos
	n_chars_number = len(str(N-1))
	n_cols_title = n_cols_term - 3 - n_chars_number
	title_format = " %%%dd: %%s"%n_chars_number
	
	# Formatação das strings a apresentar
	l_items = []
	for i in range(N):
		title = title_format%(i, l_choices[i])

		# Desconto dos caracteres grandes
		title_len_wide = 0
		title_len = 0
		for k in range(len(title)):
			char_len = 1 + (ord(title[k]) > 255)
			title_len_wide += char_len
			if title_len_wide > n_cols_term:
				break
			title_len += 1
		
		l_items.append(title[0:title_len])
	
	# Enter raw input state
	fd = sys.stdin.fileno()
	term_old = termios.tcgetattr(fd)
	term_new = termios.tcgetattr(fd)
	term_new[3] = term_new[3] & ~termios.ECHO  & ~termios.ICANON
	termios.tcsetattr(fd, termios.TCSADRAIN, term_new)
	
	# 2 space lines. One to show the running command and one space at the
	# bottom.
	lines_per_screen = n_rows_term - 2

	screen_n   = 0
	screen_old = -1
	item_n     = 0
	item_old   = -1
	line0      = 0
	line1      = 0
	
	# Start printing after the prompt
	sys.stdout.write("\n")

	while True:

		screen_n = item_n//lines_per_screen
		if screen_old != screen_n:
			screen_old = screen_n
			# Clear screen without scrolling
			sys.stdout.write("\x1b[%dA"%(line1-line0))
			sys.stdout.write("\x1b[J")

			line0 = screen_n*lines_per_screen
			line1 = min((screen_n+1)*lines_per_screen, N)
			for i in range(line0, line1):
				sys.stdout.write(l_items[i] + "\n")
			sys.stdout.flush()

		# Save home position
		sys.stdout.write("\x1b[s")
		# Move to new highlighted line.
		sys.stdout.write("\x1b[%dA"%(line1 - item_n))
		# The newline is used to move down and go to column 1 which is perfect
		# when the highlighted line is going up.
		# Also, I will not have to deal with two different ansii codes (up/down)
		sys.stdout.write("\x1b[7m" + l_items[item_n] + "\x1b[0m\n")
		# The old line must be on screen
		if item_old >= line0 and item_old < line1:
			if item_n > item_old:
				sys.stdout.write("\x1b[2A")
			sys.stdout.write(l_items[item_old] + "\n")
		# Return to home
		sys.stdout.write("\x1b[u")
		sys.stdout.flush()
		
		# GET KEY
		action = -1
		while True:
			try:
				c = sys.stdin.read(1)
			except KeyboardInterrupt:
				#FIXME codigo duplicado
				termios.tcsetattr(fd, termios.TCSADRAIN, term_old)
				exit_soft(2)
			
			if c == '\x1b':
				# Get up/down keys
				c = sys.stdin.read(1)
				if c == '[':
					c = sys.stdin.read(1)
					if c == 'A':
						action = 1
					elif c == 'B':
						action = 2
			elif c == 'k':
				action = 1
			elif c == 'j':
				action = 2
			elif c == '\n':
				action = 0
			else:
				continue
			
			break

		# ACTIONS
		if action == 0:
			break
		elif action == 1:
			if item_n != 0:
				item_old = item_n
				item_n -= 1
		elif action == 2:
			if item_n != N - 1:
				item_old = item_n
				item_n += 1

	
	# Restaurar as configurações do terminal
	termios.tcsetattr(fd, termios.TCSADRAIN, term_old)
	
	return item_n


# Procura do código correspondente aos termos de procura
# FIXME: Esta função está a fazer coisas a mais.
#        - Ela apenas devia sacar a página em função dos termos de procura e
#          obter a lista de videos encontrados.
#        - Vou ter de desenhar uma função que tome o output desta função,
#          apresente o resultado, faça a selecção e invoque o player.

def search_wordlist_to_itemId(wordlist, f_default_choice = False):
	
	# união das palavras com '+'
	str_search = "+".join(wordlist)
	
	# Conversão dos caracteres especiais em "escape characters"
	str_search = urllib.parse.quote(str_search)
	
	# Construção do url da procura
	#url_search = 'https://www.youtube.com/results?search_query=' +\
	#             str_search + '=&utm_source=opensearch'
	url_search = 'https://www.youtube.com/results?search_query=' +\
	             str_search

	html = download_page(url_search)

	l_choices = search_choices_extract(html)
	if len(l_choices) == 0:
		print("[radio-yt] No matches found")
		exit_soft(1)
	
	if f_default_choice:
		index = 0
	else:
		choices_names = [c[1] for c in l_choices]
		index         = menu_choice(choices_names)
	itemId   = l_choices[index][0]
	itemType = l_choices[index][2]

	return itemId, itemType 


def print_usage():
	print("usage: %s search_terms | youtube_url"%sys.argv[0])
	exit(2)


def check_external_dependencies():
	if not program_exist("mpv"):
		print("[radio-yt] mpv not found in system")
		exit_soft(1)
	elif not program_exist("youtube-dl"):
		print("[radio-yt] youtube-dl not found in system")
		exit_soft(1)


# Dado o input do terminal, quero obter o codigo do video
def parse_input(list_in):
	
	# Verificar se não foi introduzido input
	if len(list_in) == 1:
		print_usage()
	else:
		list_search = list_in[1:]
	
	# Flag para apenas fazer play uma vez
	f_single   = False
	# Flag para o tipo de código
	f_playlist = False

	flags_pos = {"single_play":-1, "first_match":-1, "playlist_file":-1}


# FUTURO
#	# 1) Verificar a existências de flags
#	for (i,word) in enumerate(list_search):
#		if word == "-p":
#			if f_playlist:
#				print("playlist already defined")
#				print_usage()
#			if len(list_search) == (i-1):
#				print("playlist name not found")
#				print_usage()
#			playlist_str = list_search[i]
#			f_playlist = True
#
#	# 2) Verificar a existência de urls
#
#	# Vamos processar termo por termo
#	for word in list_search:
		


	
	
	if list_search[0] == '-1':
		f_single = 1
		list_search = list_in[2:]

	if list_search[0][0:32] == 'https://www.youtube.com/watch?v=':
		# Caso seja um url
		itemId = list_search[0][32:]
	elif list_search[0][0:17] == 'https://youtu.be/':
		itemId = list_search[0][17:]
	elif list_search[0][0:30] == 'https://m.youtube.com/watch?v=':
		# ex: https://m.youtube.com/watch?v=mWvdsMwnGmM
		itemId = list_search[0][30:]
	else:
		# Procura do video
		# FIXME do this search externally to the present function
		#       this function should only parse the input, not go online and
		#       search videos.
		itemId, itemType = search_wordlist_to_itemId(list_search)
		if itemType == "pl":
			f_playlist = True

	return [itemId, f_single, f_playlist]


def print_cool_title(title):
	len_title = len(title)
	
	w_term = get_term_width()
	w_box = min(w_term, len_title + 4)
	w_txt = w_box - 4
	n_linhas = int(ceil(len_title/w_txt))
	
	#padding
	title += " "*( n_linhas*w_txt - len_title )

	print()
	print("#"*w_box)
	# Print do corpo
	for k in range(n_linhas):
		print("# " + title[k*w_txt:(k+1)*w_txt] + " #")
	print("#"*w_box)


def print_cool_title_2(title):
	print("\x1b[1;7m" + title + "\x1b[0m")


# next_vid()
# Encontra uma sugestão que ainda não tinha sido reproduzida.
# Estou a assumir que a lista de videos sugeridos é suficientemente grande para
# encontrar um vídeo ainda não reproduzido.
def next_vid(data):
	global played_vids

	debug = 0

	if debug:
		with open("/tmp/ryt_next_vid.json", "w") as fp:
			fp.write(json.dumps(data))

	
	try:
		resultsList = data\
			["contents"]\
			["twoColumnWatchNextResults"]\
			["secondaryResults"]\
			["secondaryResults"]\
			["results"]
	except KeyError:
		# Age restricted videos have no result
		print("[radio-yt] next_vid: \"results\" key not found")
		return None
	
	videoId = None
	for result in resultsList:
		k = list(result.keys())[0]
		
		# TODO 2021-03-25
		# youtube seems to have abandoned the "compactAutoplayRenderer" entry
		# Maybe it can be eliminated
		if k == "compactAutoplayRenderer":
			if debug:
				print("FIRST RESULT > ", end="")
			videoId = result\
			[k]\
			["contents"]\
			[0]\
			["compactVideoRenderer"]\
			["videoId"]
		elif k == "compactVideoRenderer":
			if debug:
				print("NEXT > ", end="")
			videoId = result[k]["videoId"]
		else:
			if debug:
				print("UNKNOWN (" + k + ") > ", end="")
			continue
		
		if videoId not in played_vids:
			break
	if debug:
		print()

	return videoId



# play_vid()
# Play do video correspondente ao videoId.
# Retorna o videoId do próximo vídeo
def play_vid(videoId):
	global thumb_fn
	
	# Se código for um None, o programa pára aqui.
	# A função é chamada com um None, se a página do video anterior não tiver
	# nenhuma sugestão. A função não irá retornar um código None, porque isso
	# iria criar um loop infinito de chamadas ao play_vid.
	# As playlists não têm esse problema, porque ignoram os códigos encontrados.
	# FIXME: Penso que a procura de códigos devesse estar fora desta função
	# de modo ter a lógica fora desta, focando-se apenas no mecanismo de play.
	if videoId == None:
		print("[radio-yt] play_vid: videoId = None")
		exit_soft(1)

	# Variáveis internas
	# status -> devolve códigos do funcionamento
	#           0: ok
	#           1: video not found
	status = 0
	
	# Actualização da lista de videos já reproduzidos.
	#FIXME variáveis globais??? Será que deveria ter um mecanismo exterior que
	# lidasse com esta mini base de dados?
	global played_vids
	played_vids += [videoId]

	url     = videoId2url(videoId)
	img_url = itemId2url_img(videoId)
	
	html = download_page(url)
	data = html_get_datajson(html)
	
	title = data["contents"]["twoColumnWatchNextResults"]["results"]["results"]\
		["contents"][0]["videoPrimaryInfoRenderer"]["title"]["runs"][0]["text"]
	
	# Vou assumir que quando o vídeo não tem título, o vídeo não existe
	if title == "":
		print("[radio_yt] Title not found. Exiting.\n")
		return None, 1

	# Apresentação do nome na linha de comandos
	#print_cool_title(title)
	print()
	print_cool_title_2(title)

	# Apresentação do url
	print(url)
	print()
	
	# Thumbnail : download e apresentação
	if program_exist("notify-send"):
		download_file(img_url, thumb_fn)
		cmd_list = ["notify-send", "-i", thumb_fn, title]
		run_process(cmd_list, True)

	mpv_opt = ["--no-video"]
	try:
		sp.run(["mpv"] + mpv_opt + [url])
	except KeyboardInterrupt:
		print()
		#FIXME: return ou exit?
		# Tenho mensagens de erro espalhada pelo código.
		exit_soft(1)
	
	# Separação para o próximo vídeo
	print("\n")

	# Próximo video
	videoId = next_vid(data)

	return videoId, status


def get_playlist_videos(playlistId):
	url  = videoId2url(playlistId)
	html = download_page(url)
	
	# Procura do início da playlist
	i0 = html.find("playlist-videos-container")
	if i0 == -1:
		return []

	playlist = []

	while True:
		i0 = html.find("yt-uix-scroller-scroll-unit  vve-check", i0)
		if i0 == -1:
			break
		i0 = html.find("href=", i0) + 15
		playlist += [html[i0:i0+11]]
		
	return playlist


# O input tanto pode ser uma lista de itemId, como uma lista de termos.
def play_playlist(playlist, f_search=False):
	N = len(playlist)
	print()
	for i, entry in enumerate(playlist):
		print("[radio-yt] playlist item %i of %i"%(i+1,N))
		if f_search:
			itemId, itemType = search_wordlist_to_itemId(entry, True)
		else:
			itemId = entry
		play_vid(itemId)


################################################################################
################################################################################

# Prioridade de input:
# Vou atribuir prioridade aos inputs do stdin

# Lista dos videos já reproduzidos
global played_vids
played_vids = []
# Path da imagem do thumbnail
global thumb_fn
thumb_fn = thumb_fn_new()


# EXTERNAL PROGRAMS VERIFICATION
check_external_dependencies()


# TODO
# Eu quero dois inputs novos:
# - '-p' para indicar um ficheiro "playlist"
# - '-f' para indicar que quer a primeira match que aparecer


# Parsing do input
itemId, f_single, f_playlist = parse_input(sys.argv)

if f_single:
	_, status = play_vid(itemId)
	exit_soft(status)

elif f_playlist:
	playlist = get_playlist_videos(itemId)
	play_playlist(playlist)
	exit_soft(0)

while 1:
	itemId, status = play_vid(itemId)
	if status:
		exit_soft(status)
