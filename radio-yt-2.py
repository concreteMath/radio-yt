#!/usr/bin/python3

# This version uses an state machine
# The central object is a list with commands and parameters
# Each command adds new commands

import sys
import os
import urllib.request
import urllib
import json
import shutil
import termios
import subprocess as sp


# EXIT
# Cleanup and exit
def exit_soft(exit_code):
	#global thumb_fn
	#if os.path.exists(thumb_fn):
	#	os.remove(thumb_fn)
	print("[radio-yt] Goodbye")
	exit(exit_code)


def print_usage():
	print("usage: %s search_terms | youtube_url"%sys.argv[0])


# REMOTE
def download_page(url):
	page = urllib.request.urlopen(url)
	html = page.read().decode()
	return html


def download_file(url, fn):
	r = urllib.request.urlopen(url)
	binary = r.read()
	fp = open(fn, "wb")
	fp.write(binary)
	fp.close()


# JSON
def html_get_datajson(html):
	beginStr = ">var ytInitialData = {"
	endStr   = "};</script>"

	i = html.index(beginStr)
	if i == -1:
		return None
	i0 = i + len(beginStr) - 1
	
	i1 = -1
	# Bracket count
	bc = 1
	for k in range(i0+1, len(html)):
		if bc == 0:
			i1 = k
			break
		if html[k] == '}':
			bc -= 1
		elif html[k] == '{':
			bc += 1
	if i1 == -1:
		return None

	data = json.loads(html[i0:i1])
	return data



# MENU

# Input : String list
# Output: Chosen item index
def menu_choice(l_choices):
	N = len(l_choices)

	if N==0:
		return None

	n_cols_term, n_rows_term = shutil.get_terminal_size()

	# 3 columas = 2 espaço + 1 char de dois pontos
	n_chars_number = len(str(N-1))
	n_cols_title = n_cols_term - 3 - n_chars_number
	title_format = " %%%dd: %%s"%n_chars_number
	
	# Formatação das strings a apresentar
	l_items = []
	for i in range(N):
		title = title_format%(i, l_choices[i])

		# Desconto dos caracteres grandes
		title_len_wide = 0
		title_len = 0
		for k in range(len(title)):
			char_len = 1 + (ord(title[k]) > 255)
			title_len_wide += char_len
			if title_len_wide > n_cols_term:
				break
			title_len += 1
		
		l_items.append(title[0:title_len])
	
	# Enter raw input state
	fd = sys.stdin.fileno()
	term_old = termios.tcgetattr(fd)
	term_new = termios.tcgetattr(fd)
	term_new[3] = term_new[3] & ~termios.ECHO  & ~termios.ICANON
	termios.tcsetattr(fd, termios.TCSADRAIN, term_new)
	
	# 2 space lines. One to show the running command and one space at the
	# bottom.
	lines_per_screen = n_rows_term - 2

	screen_n   = 0
	screen_old = -1
	item_n     = 0
	item_old   = -1
	line0      = 0
	line1      = 0
	
	# Start printing after the prompt
	sys.stdout.write("\n")

	while True:

		screen_n = item_n//lines_per_screen
		if screen_old != screen_n:
			screen_old = screen_n
			# Clear screen without scrolling
			sys.stdout.write("\x1b[%dA"%(line1-line0))
			sys.stdout.write("\x1b[J")

			line0 = screen_n*lines_per_screen
			line1 = min((screen_n+1)*lines_per_screen, N)
			for i in range(line0, line1):
				sys.stdout.write(l_items[i] + "\n")
			sys.stdout.flush()

		# Save home position
		sys.stdout.write("\x1b[s")
		# Move to new highlighted line.
		sys.stdout.write("\x1b[%dA"%(line1 - item_n))
		# The newline is used to move down and go to column 1 which is perfect
		# when the highlighted line is going up.
		# Also, I will not have to deal with two different ansii codes (up/down)
		sys.stdout.write("\x1b[7m" + l_items[item_n] + "\x1b[0m\n")
		# The old line must be on screen
		if item_old >= line0 and item_old < line1:
			if item_n > item_old:
				sys.stdout.write("\x1b[2A")
			sys.stdout.write(l_items[item_old] + "\n")
		# Return to home
		sys.stdout.write("\x1b[u")
		sys.stdout.flush()
		
		# GET KEY
		action = -1
		while True:
			try:
				c = sys.stdin.read(1)
			except KeyboardInterrupt:
				#FIXME codigo duplicado
				termios.tcsetattr(fd, termios.TCSADRAIN, term_old)
				exit_soft(2)
			
			if c == '\x1b':
				# Get up/down keys
				c = sys.stdin.read(1)
				if c == '[':
					c = sys.stdin.read(1)
					if c == 'A':
						action = 1
					elif c == 'B':
						action = 2
			elif c == 'k':
				action = 1
			elif c == 'j':
				action = 2
			elif c == '\n':
				action = 0
			else:
				continue
			
			break

		# ACTIONS
		if action == 0:
			break
		elif action == 1:
			if item_n != 0:
				item_old = item_n
				item_n -= 1
		elif action == 2:
			if item_n != N - 1:
				item_old = item_n
				item_n += 1

	
	# Restaurar as configurações do terminal
	termios.tcsetattr(fd, termios.TCSADRAIN, term_old)
	
	return item_n


# SEARCH
def search_choices_extract(html):
	data = html_get_datajson(html)

	resultList= data\
		["contents"]\
		["twoColumnSearchResultsRenderer"]\
		["primaryContents"]\
		["sectionListRenderer"]\
		["contents"]\
		[0]\
		["itemSectionRenderer"]\
		["contents"]

	l_choices = []
	for v in resultList:
		k = list(v.keys())[0]
		if k == "videoRenderer":
			videoId   = v[k]["videoId"]
			title     = v[k]["title"]["runs"][0]["text"]
			l_choices.append([videoId, title])

	return l_choices



# VIDEO

# next_vid()
# Encontra uma sugestão que ainda não tinha sido reproduzida.
# Estou a assumir que a lista de videos sugeridos é suficientemente grande para
# encontrar um vídeo ainda não reproduzido.
def videoPage_get_nextVideo(data):
	global played_vids

	debug = 0

	if debug:
		with open("/tmp/ryt_next_vid.json", "w") as fp:
			fp.write(json.dumps(data))

	
	try:
		resultsList = data\
			["contents"]\
			["twoColumnWatchNextResults"]\
			["secondaryResults"]\
			["secondaryResults"]\
			["results"]
	except KeyError:
		# Age restricted videos have no result
		print("[radio-yt] next_vid: \"results\" key not found")
		return None
	
	videoId = None
	for result in resultsList:
		k = list(result.keys())[0]
		
		# TODO 2021-03-25
		# youtube seems to have abandoned the "compactAutoplayRenderer" entry
		# Maybe it can be eliminated
		if k == "compactAutoplayRenderer":
			if debug:
				print("FIRST RESULT > ", end="")
			videoId = result\
			[k]\
			["contents"]\
			[0]\
			["compactVideoRenderer"]\
			["videoId"]
		elif k == "compactVideoRenderer":
			if debug:
				print("NEXT > ", end="")
			videoId = result[k]["videoId"]
		else:
			if debug:
				print("UNKNOWN (" + k + ") > ", end="")
			continue
		
		if videoId not in played_vids:
			break
	if debug:
		print()

	return videoId



def video_make_url(videoId):
	return "https://www.youtube.com/watch?v=" + videoId


def video_print_begin(video_title, video_url):
	print()
	print("\x1b[1;7m" + video_title + "\x1b[0m")
	print(video_url)
	print()


def video_print_end():
	# Separação para o próximo vídeo
	print("\n")


def video_mpv_play(url):
	mpv_opt = ["--no-video"]
	try:
		sp.run(["mpv"] + mpv_opt + [url])
	except KeyboardInterrupt:
		print()
		#FIXME: return ou exit?
		# Tenho mensagens de erro espalhada pelo código.
		exit_soft(1)
	

def video_onlineData(videoId):
	url = video_make_url(videoId)
	html = download_page(url)

	data = html_get_datajson(html)
	title = data["contents"]["twoColumnWatchNextResults"]["results"]["results"]\
		["contents"][0]["videoPrimaryInfoRenderer"]["title"]["runs"][0]["text"]
	nextVideo_videoId = videoPage_get_nextVideo(data)

	v_data = {
		"url"      : url,
		"data"     : data,
		"title"    : title,
		"nextVideo": nextVideo_videoId
	}
	return v_data


def video_thumbnail_get(videoId):
	# Generate url
	thumb_url = "https://i.ytimg.com/vi/"+ videoId + "/hqdefault.jpg"
	# Generate destination path
	pid = os.getpid()
	thumb_fn = "/tmp/hqdefault-%i.jpg"%pid
	# Download file
	download_file(thumb_url, thumb_fn)
	return thumb_fn


def video_thumbnail_show(thumb_fn, title):
	cmd = ["notify-send", "-i", thumb_fn, title]
	sp.run(cmd)



# COMMANDS

def cmd_search(wordlist):
	# Join word_list with '+'
	str_search = "+".join(wordlist)
	# Escape special characters
	str_search = urllib.parse.quote(str_search)
	# Build search URL
	url_search = 'https://www.youtube.com/results?search_query=' + str_search
	# Download search page
	html = download_page(url_search)
	# Extract choices
	l_choices = search_choices_extract(html)
	return l_choices


def cmd_choice(l_choice):
	# Extract titles
	titles = [c[1] for c in l_choice]
	# Make a choice
	vid_index = menu_choice(titles)
	# Obtain videoId
	videoId = l_choice[vid_index][0]
	return videoId


def cmd_play(videoId):
	global played_vids
	# Get online data
	v_data = video_onlineData(videoId)
	# Get thumbnail
	v_thumb_fn = video_thumbnail_get(videoId)
	# Show thumbnail
	video_thumbnail_show(v_thumb_fn, v_data["title"])
	# Print info
	video_print_begin(v_data["title"], v_data["url"])
	# Play
	video_mpv_play(v_data["url"])
	# Print more info
	video_print_end()
	# Update list of already played videos
	played_vids.append(videoId)
	return v_data["nextVideo"]
	


# INPUT PARSER
# Returns a cmd_stack, according with input
# FIXME: maybe it should process the an input, like a pure function
# This function just prepares the initial state to be processed by the main
# loop
def input_parse():
	args = sys.argv

	# Remove program name
	args = args[1:]

	cmd_stack = [["CMD_EXIT", None]]

	# Empty arguments will call usage
	if len(args) == 0:
		cmd_stack.append(["CMD_USAGE", None])
		return cmd_stack

	# Check for just one play or infinite loop
	if args[0] == '-1':
		args = args[1:]
	else:
		cmd_stack.append(["CMD_NEXT", None])
	
	# Check if is URL
	itemId = None
	if args[0][0:32] == 'https://www.youtube.com/watch?v=':
		# Caso seja um url
		itemId = args[0][32:]
	elif args[0][0:28] == 'https://youtube.com/watch?v=':
		itemId = args[0][28:]
	elif args[0][0:17] == 'https://youtu.be/':
		itemId = args[0][17:]
	elif args[0][0:30] == 'https://m.youtube.com/watch?v=':
		# ex: https://m.youtube.com/watch?v=mWvdsMwnGmM
		itemId = args[0][30:]
	
	# If is not an URL, prepare to search
	if itemId != None:
		cmd_stack.append(["CMD_PLAY", itemId])
	else:
		cmd_stack.append(["CMD_ADD_CHOICE", None])
		cmd_stack.append(["CMD_SEARCH", args])
	
	return cmd_stack





# MAIN

def main_loop(cmd_stack):
	# Loop variables
	vid_next = None
	playlist = None
	
	while True:
		if len(cmd_stack) == 0:
			exit(2)
		cmd, arg = cmd_stack.pop()

		if cmd == "CMD_EXIT":
			exit_soft(1)

		elif cmd == "CMD_USAGE":
			print_usage()

		elif cmd == "CMD_SEARCH":
			playlist = cmd_search(arg)
			# FIXME Should error handling be here?
			if len(playlist) == 0:
				cmd_stack.append(["CMD_EXIT", None])

		elif cmd == "CMD_ADD_FIRST":
			vid_next = suggestions[0]
			cmd_stack.append(["CMD_PLAY", vid_next])

		elif cmd == "CMD_ADD_CHOICE":
			vid_next = cmd_choice(playlist)
			cmd_stack.append(["CMD_PLAY", vid_next])

		elif cmd == "CMD_PLAY":
			vid_next = cmd_play(arg)

		elif cmd == "CMD_NEXT":
			cmd_stack.append(["CMD_NEXT", None])
			cmd_stack.append(["CMD_PLAY", vid_next])

		elif cmd == "CMD_PLAYLIST":
			pl = playlist_getlist(arg)
			# Push all videos to playlist by reverse order

		else:
			print(cmd, "cmd not understood")
			exit(3)


# GENERAL
global played_vids
played_vids = []

global global_config


cmd_stack = input_parse()
main_loop(cmd_stack)
