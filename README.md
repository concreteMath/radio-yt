## radio-yt
### A youtube audio player with auto-play

Just do
```
radio-yt $search_terms
```
or
```
radio_yt $youtube_url
```
and start listening to the youtube stream.



Dependencies:
```
python3
mpv
youtube-dl
notify-send
```
v5.5
- Can now process new youtube search results source code
- Clean dead code

v5.4.3
- Fixed a bug in the `next_vid` function, which broke when the code of the next video could not be found

v5.4.2
- Fixed youtube short url acceptance

v5.4
- Removed various dependencies
- Added playlist playback

v5.3.2
- Correction of the `play_vid()` function. (The program was trying to play no-existing videos, returning empty titles, generating the bug in v5.3.1)

v5.3.1
- Divide-by-zero bug correction in function `print_cool_title()`

