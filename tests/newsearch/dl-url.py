#!/usr/bin/python3
#!/usr/bin/python3


import sys
import urllib.request

def download_page(url):
	page = urllib.request.urlopen(url)
	html = page.read().decode()
	return html

url = sys.argv[1]
html = download_page(url)
sys.stdout.write(html)
