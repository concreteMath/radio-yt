#!/usr/bin/python3

import sys
import json


def html_get_datajson(html):
	html_lines = html.split("\n")
	for l in html_lines:
		# Ad-hoc prefix location within the first 100 char
		if "window[\"ytInitialData\"] = " not in l[:100]:
			continue
		i0 = l.find("{")
		i1 = l.rfind("}")
		data_raw = l[i0:i1+1]
	
	data = json.loads(data_raw)
	return data

html = sys.stdin.read()
data = html_get_datajson(html)
datastr = json.dumps(data)
sys.stdout.write(datastr)
