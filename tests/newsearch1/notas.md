2020-11-27

O html tanto da página do vídeo como da procura mudaram.

Estive a analisar o html do video e descobri um link perto da tag `<title>` que dá acesso à API json do youtube, a qual pode ser usada para ir buscar informação básica como o título do vídeo.
Exemplo:
`http://www.youtube.com/oembed?format=json&amp;url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D98UjjwzJBFE`

As sugestões também estão no html da página dentro de um json, portanto, é uma questão de encontrar o início e fim do json, fazer parsing e sacar os videoId das sugestões.

Penso que a informação sobre o vídeo e as sugestões comecem a seguir à string `var ytInitialData = {"` e acabem na tag `</script>`. A posição inicial também pode ser procurada pela tag de abertura `<script ... >`.

Parece-me que a diferença entre a página antiga e página nova é que as informações estão fora do sítio.
Numa inspecção mais rigorosa, a estrutura do json das sugestões também mudou um bocado, portanto o parser que saca a lista de sugestões também tem de ser alterado.

--------------------------------------------------------------------------------
2020-12-20

Comecei por sacar a página do "last christmas" do wham.
Agora vou criar uma função de procura do título e outra de procura da próximo vídeo.
