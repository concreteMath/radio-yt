import json

# FIXME
def html_get_datajson(html):
	#html_lines = html.split("\n")
	beginStr = ">var ytInitialData = {"
	endStr   = "};</script>"

	i = html.index(beginStr)
	if i == -1:
		print("NOT COOL")
	i0 = i + len(beginStr) - 1
	
	# Search end of json
	#i1 = html.index(endStr, i0) + 1
	
	i1 = -1
	# Bracket count
	bc = 1
	for k in range(i0+1, len(html)):
		if bc == 0:
			i1 = k
			break
		if html[k] == '}':
			bc -= 1
		elif html[k] == '{':
			bc += 1
	if i1 == -1:
		print("NOT COOL 1")

	data = json.loads(html[i0:i1])
	return data


def search_getResults(data):
	resList = data\
		["contents"]\
		["twoColumnSearchResultsRenderer"]\
		["primaryContents"]\
		["sectionListRenderer"]\
		["contents"]\
		[0]\
		["itemSectionRenderer"]\
		["contents"]
	
	resOut = []
	for i in range(len(resList)):
		print(i)
		if "videoRenderer" in resList[i]:
			videoId = resList[i]["videoRenderer"]["videoId"]
			title   = resList[i]["videoRenderer"]["title"]["runs"][0]["text"]
			resOut.append([videoId, title, "vd"])
			#print(videoId, title)
		elif "radioRenderer" in resList[i]:
			videoId = resList[i]["radioRenderer"]["playlistId"]
			title   = resList[i]["radioRenderer"]["title"]["simpleText"]
			#print(videoId, title)
		else:
			print("nope")
	
	return resOut

	
#def video_getDetails(data):
#	.contents.twoColumnWatchNextResults.results.results.contents[0].#videoPrimaryInfoRenderer.title.runs[0].text



def search_wordlist_to_code(wordlist, f_default_choice = False):
	
	# união das palavras com '+'
	str_search = "+".join(wordlist)
	
	# Conversão dos caracteres especiais em "escape characters"
	str_search = urllib.parse.quote(str_search)
	
	# Construção do url da procura
	#url_search = 'https://www.youtube.com/results?search_query=' +\
	#             str_search + '=&utm_source=opensearch'
	url_search = 'https://www.youtube.com/results?search_query=' +\
	             str_search

	html = download_page(url_search)

	l_choices = search_choices_extract(html)
	if len(l_choices) == 0:
		print("[radio-yt] No matches found")
		exit_soft(1)
	
	if f_default_choice:
		index = 0
	else:
		#code, code_type = user_make_choice(l_choices)
		choices_names = [c[1] for c in l_choices]
		index         = menu_choice(choices_names)
	code          = l_choices[index][0]
	code_type     = l_choices[index][2]

	return code, code_type


#fn = "/tmp/ryt-page-search.html"
fn = "/tmp/ryt-page.html"
with open(fn) as fp:
	html = fp.read()


print(json.dumps(html_get_datajson(html)))
#print(search_getResults(html_get_datajson(html)))
