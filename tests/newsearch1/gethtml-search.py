import urllib.request

def download_page(url):
	page = urllib.request.urlopen(url)
	html = page.read().decode()
	return html

def download_file(url, filename):
	r = urllib.request.urlopen(url)
	binary = r.read()
	fp = open(filename, "wb")
	fp.write(binary)
	fp.close()

# Last christmas
url_search = 'https://www.youtube.com/results?search_query=wham+last+christmas'
download_file(url_search, "/tmp/ryt-page-search.html")
