import urllib.request

def download_page(url):
	page = urllib.request.urlopen(url)
	html = page.read().decode()
	return html

def download_file(url, filename):
	r = urllib.request.urlopen(url)
	binary = r.read()
	fp = open(filename, "wb")
	fp.write(binary)
	fp.close()

# Last christmas
url="https://www.youtube.com/watch?v=-YUH8Xfz-jg"

download_file(url, "/tmp/ryt-page.html")
